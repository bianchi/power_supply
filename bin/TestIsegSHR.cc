/*!
 * \author Massimiliano Marchisone <mmarchis.@cern.ch>
 * \date Jul 30 2020
 */

// Libraries
#include "DeviceHandler.h"
#include "PowerSupply.h"
#include "PowerSupplyChannel.h"
#include <boost/program_options.hpp> //!For command line arg parsing
#include <fstream>
#include <iomanip>
#include <iostream>
// using namespace::std;
#include <algorithm>
#include <limits>
#include <unistd.h>

// Custom Libraries
#include "IsegSHR.h"

// #include "Keithley2000.h"
// #include "KeithleyMultimeter.h"

// Namespaces
using namespace std;
namespace po = boost::program_options;

/*!
************************************************
* A simple "wait for input".
************************************************
*/
void wait()
{
    cout << "Press ENTER to continue...";
    cin.ignore(numeric_limits<streamsize>::max(), '\n');
}

/*!
************************************************
* Argument parser.
************************************************
*/
po::variables_map process_program_options(const int argc, const char* const argv[])
{
    po::options_description desc("Allowed options");

    desc.add_options()("help,h", "produce help message")("config,c",
                                                         po::value<string>()->default_value("default"),
                                                         "set configuration file path (default files defined for each test) "
                                                         "...")("verbose,v", po::value<string>()->implicit_value("0"), "verbosity level");

    po::variables_map vm;
    try
    {
        po::store(po::parse_command_line(argc, argv, desc), vm);
    }
    catch(po::error const& e)
    {
        std::cerr << e.what() << '\n';
        exit(EXIT_FAILURE);
    }
    po::notify(vm);

    // Help
    if(vm.count("help"))
    {
        cout << desc << "\n";
        return 0;
    }

    // Power supply object option
    if(vm.count("object")) { cout << "Object to initialize set to " << vm["object"].as<string>() << endl; }

    // Test type execution
    // if (vm.count("test"))
    //   test(vm["test"].as<string>(),vm["config"].as<string>(),vm.count("verbose"));
    return vm;
}

/*!
************************************************
* Reading module identification.
************************************************
*/
void ModuleTest(IsegSHRChannel* IsegSHRchannel)
{
    cout << "Asking for device ID" << endl;
    cout << IsegSHRchannel->getDeviceID() << endl;
    cout << "Asking for module temperature" << endl;
    cout << IsegSHRchannel->getDeviceTemperature() << endl;
}

/*!
************************************************
* Test of HV power cycle.
************************************************
*/
void OnOffTest(IsegSHRChannel* IsegSHRchannel)
{
    cout << "switching on" << endl;
    IsegSHRchannel->turnOn();
    cout << IsegSHRchannel->isOn() << endl;
    wait();
    cout << "switching off" << endl;
    IsegSHRchannel->turnOff();
    cout << IsegSHRchannel->isOn() << endl;
    wait();
    cout << "switching off" << endl;
    IsegSHRchannel->turnOff();
    cout << IsegSHRchannel->isOn() << endl;
    wait();
    cout << "switching on" << endl;
    IsegSHRchannel->turnOn();
    cout << IsegSHRchannel->isOn() << endl;
    wait();
    cout << "switching off" << endl;
    IsegSHRchannel->turnOff();
    cout << IsegSHRchannel->isOn() << endl;
    wait();
    cout << "switching on" << endl;
    IsegSHRchannel->turnOn();
    cout << IsegSHRchannel->isOn() << endl;
    wait();
    cout << "switching on" << endl;
    IsegSHRchannel->turnOn();
    cout << IsegSHRchannel->isOn() << endl;
    wait();
    cout << "switching off" << endl;
    IsegSHRchannel->turnOff();
    cout << IsegSHRchannel->isOn() << endl;
}

/*!
************************************************
* Test of polarity switch.
************************************************
*/
void PolarityTest(IsegSHRChannel* IsegSHRchannel)
{
    cout << "Turning off" << endl;
    IsegSHRchannel->turnOff();
    wait();
    cout << "setting polarity to N" << endl;
    IsegSHRchannel->setPolarity("n");
    cout << IsegSHRchannel->getPolarity() << endl;
    wait();
    cout << "setting polarity to P" << endl;
    IsegSHRchannel->setPolarity("p");
    cout << IsegSHRchannel->getPolarity() << endl;
    wait();
    cout << "setting polarity to N" << endl;
    IsegSHRchannel->setPolarity("n");
    cout << IsegSHRchannel->getPolarity() << endl;
    wait();
    cout << "setting polarity to N" << endl;
    IsegSHRchannel->setPolarity("n");
    cout << IsegSHRchannel->getPolarity() << endl;
    wait();
    cout << "setting polarity to P" << endl;
    IsegSHRchannel->setPolarity("p");
    cout << IsegSHRchannel->getPolarity() << endl;
    wait();
    cout << "setting polarity to N" << endl;
    IsegSHRchannel->setPolarity("n");
    cout << IsegSHRchannel->getPolarity() << endl;
    wait();
    cout << "setting polarity to P" << endl;
    IsegSHRchannel->setPolarity("p");
    cout << IsegSHRchannel->getPolarity() << endl;
    wait();
    cout << "setting polarity to P" << endl;
    IsegSHRchannel->setPolarity("p");
    cout << IsegSHRchannel->getPolarity() << endl;
    wait();
    cout << "setting polarity to N" << endl;
    IsegSHRchannel->setPolarity("n");
    cout << IsegSHRchannel->getPolarity() << endl;
    wait();
}

/*!
************************************************
* Test of voltage settings and readings.
************************************************
*/
void VoltageTest(IsegSHRChannel* IsegSHRchannel)
{
    cout << "Setting polarity to N" << endl;
    IsegSHRchannel->setPolarity("n");
    cout << IsegSHRchannel->getPolarity() << endl;
    wait();
    cout << "Turning on" << endl;
    IsegSHRchannel->turnOn();
    wait();
    cout << "Setting voltage to 0 V" << endl;
    IsegSHRchannel->setVoltage(0);
    cout << IsegSHRchannel->getSetVoltage() << endl;
    wait();
    cout << IsegSHRchannel->getVoltage() << endl;
    wait();
    cout << "Setting voltage to -100 V" << endl;
    IsegSHRchannel->setVoltage(-100);
    cout << IsegSHRchannel->getSetVoltage() << endl;
    wait();
    cout << IsegSHRchannel->getVoltage() << endl;
    wait();
    cout << "Setting voltage to -200 V" << endl;
    IsegSHRchannel->setVoltage(-200);
    cout << IsegSHRchannel->getSetVoltage() << endl;
    wait();
    cout << IsegSHRchannel->getVoltage() << endl;
    wait();
    cout << "Setting voltage to -300 V" << endl;
    IsegSHRchannel->setVoltage(-300);
    cout << IsegSHRchannel->getSetVoltage() << endl;
    wait();
    cout << IsegSHRchannel->getVoltage() << endl;
    wait();
    cout << "Setting voltage to 0 V" << endl;
    IsegSHRchannel->setVoltage(0);
    cout << IsegSHRchannel->getSetVoltage() << endl;
    wait();
    cout << IsegSHRchannel->getVoltage() << endl;
    wait();
    cout << "Turning off" << endl;
    IsegSHRchannel->turnOff();
    wait();
    cout << "Setting polarity to P" << endl;
    IsegSHRchannel->setPolarity("p");
    cout << IsegSHRchannel->getPolarity() << endl;
    wait();
    cout << "Turning on" << endl;
    IsegSHRchannel->turnOn();
    wait();
    cout << "Setting voltage to +100 V" << endl;
    IsegSHRchannel->setVoltage(100);
    cout << IsegSHRchannel->getSetVoltage() << endl;
    wait();
    cout << IsegSHRchannel->getVoltage() << endl;
    wait();
    cout << "Setting voltage to +200 V" << endl;
    IsegSHRchannel->setVoltage(200);
    cout << IsegSHRchannel->getSetVoltage() << endl;
    wait();
    cout << IsegSHRchannel->getVoltage() << endl;
    wait();
    cout << "Setting voltage to +300 V" << endl;
    IsegSHRchannel->setVoltage(300);
    cout << IsegSHRchannel->getSetVoltage() << endl;
    wait();
    cout << IsegSHRchannel->getVoltage() << endl;
    wait();
    cout << "Setting voltage to 0 V" << endl;
    IsegSHRchannel->setVoltage(0);
    cout << IsegSHRchannel->getSetVoltage() << endl;
    wait();
    cout << IsegSHRchannel->getVoltage() << endl;
    wait();
    cout << "Turning off" << endl;
    IsegSHRchannel->turnOff();
    wait();
    cout << "Setting voltage limits" << endl;
    IsegSHRchannel->setVoltageCompliance(123);
    wait();
    IsegSHRchannel->setVoltageCompliance(456);
    wait();
    IsegSHRchannel->setOverVoltageProtection(4519);
    wait();
    IsegSHRchannel->setOverVoltageProtection(-4519);
    wait();
    cout << "Setting voltage limits" << endl;
    cout << IsegSHRchannel->getVoltageCompliance() << endl;
    wait();
    cout << IsegSHRchannel->getOverVoltageProtection() << endl;
    wait();
    cout << IsegSHRchannel->getVoltageCompliance() << endl;
    wait();
    cout << IsegSHRchannel->getOverVoltageProtection() << endl;
    wait();
}

void ChannelStatusTest(IsegSHRChannel* IsegSHRchannel)
{
    cout << "Setting polarity to N" << endl;
    IsegSHRchannel->setPolarity("n");
    cout << IsegSHRchannel->getPolarity() << endl;
    wait();
    cout << "Setting voltage to 0 V" << endl;
    IsegSHRchannel->setVoltage(0);
    cout << IsegSHRchannel->getSetVoltage() << endl;
    wait();
    cout << "Turning on" << endl;
    IsegSHRchannel->turnOn();
    wait();
    cout << "Setting voltage to -150 V" << endl;
    IsegSHRchannel->setVoltage(-150);
    cout << IsegSHRchannel->getSetVoltage() << endl;
    wait();
    for(int i = 0; i < 10; i++)
    {
        cout << IsegSHRchannel->getChannelStatus() << "   " << IsegSHRchannel->isRamping() << "   " << IsegSHRchannel->getVoltage() << endl;
        wait();
    }

    cout << "Turning off" << endl;
    IsegSHRchannel->turnOff();
    wait();
    for(int i = 0; i < 10; i++)
    {
        cout << IsegSHRchannel->getChannelStatus() << "   " << IsegSHRchannel->isRamping() << "   " << IsegSHRchannel->getVoltage() << endl;
        wait();
    }
    cout << "Setting voltage to 0 V" << endl;
    IsegSHRchannel->setVoltage(0);
    cout << IsegSHRchannel->getSetVoltage() << endl;
    wait();
    cout << IsegSHRchannel->getChannelStatus() << endl;
}

/*!
************************************************
* Test of ramp-up and ramp-down settings.
************************************************
*/
void RampTest(IsegSHRChannel* IsegSHRchannel) {}

/*!
************************************************
* Test of current settings and readings.
************************************************
*/
void CurrentTest(IsegSHRChannel* IsegSHRchannel) { cout << IsegSHRchannel->getCurrent() << endl; }

/*!
************************************************
* Test of reset command (turn all HV off, set all HV to 0, set all currents to nominal).
************************************************
*/
void ResetTest(IsegSHRChannel* IsegSHRchannel) {}

/*!
************************************************
* I vs V curve.
************************************************
*/
void IVCurve(IsegSHRChannel* IsegSHRchannel)
{
    const useconds_t sleeping_time    = 1e6; // allowed range [0,1e6]
    const int        nPoint           = 7;
    float            valuesHV[nPoint] = {0., -50., -100., -150., -200., -250., -300.};
    float            readCurrent[nPoint];

    if(IsegSHRchannel->getPolarity() != "n")
    {
        IsegSHRchannel->setPolarity("n");
        usleep(sleeping_time);
    }

    if(IsegSHRchannel->getSetVoltage() != 0.)
    {
        IsegSHRchannel->setVoltage(0.);
        usleep(sleeping_time);
    }

    if(IsegSHRchannel->isOn() == 0)
    {
        IsegSHRchannel->turnOn();
        usleep(sleeping_time);
    }

    for(int iPoint = 0; iPoint < nPoint; iPoint++)
    {
        IsegSHRchannel->setVoltage(valuesHV[iPoint]);

        while(IsegSHRchannel->isRamping()) usleep(sleeping_time);

        usleep(sleeping_time);

        readCurrent[iPoint] = IsegSHRchannel->getCurrent();
        cout << readCurrent[iPoint] << endl;
        usleep(sleeping_time);
    }
}

/*!
 ************************************************
 * Main.
 ************************************************
 */
int main(int argc, char* argv[])
{
    boost::program_options::variables_map v_map = process_program_options(argc, argv);
    cout << "Initializing ..." << endl;

    std::string        docPath = v_map["config"].as<string>();
    pugi::xml_document docSettings;

    DeviceHandler theHandler;
    theHandler.readSettings(docPath, docSettings);

    try
    {
        theHandler.getPowerSupply("MyIsegSHR4220");
    }
    catch(const std::out_of_range& oor)
    {
        std::cerr << "Out of Range error: " << oor.what() << '\n';
    }

    PowerSupply*        PS              = theHandler.getPowerSupply("MyIsegSHR4220");
    IsegSHR*            myIsegSHR       = dynamic_cast<IsegSHR*>(PS);
    PowerSupplyChannel* channel1        = myIsegSHR->getChannel("HV_Module1"); // ID = 0
    IsegSHRChannel*     IsegSHRchannel1 = dynamic_cast<IsegSHRChannel*>(channel1);

    cout << "---------------------------------------------------" << endl;

    //  cout << "Asking for measured current" << endl;
    //  ModuleTest(IsegSHRchannel1);
    //  OnOffTest(IsegSHRchannel1);
    //  PolarityTest(IsegSHRchannel1);
    //  VoltageTest(IsegSHRchannel1);
    //  ChannelStatusTest(IsegSHRchannel1);
    RampTest(IsegSHRchannel1); // TODO
    //  CurrentTest(IsegSHRchannel1); //TODO
    //  ResetTest(IsegSHRchannel1); //TODO
    //  IVCurve(IsegSHRchannel1); //TODO

    return 0;
}
